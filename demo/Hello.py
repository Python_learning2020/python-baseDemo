# !/usr/bin/python

print("Hello world");


# -*- coding: UTF-8 -*-

counter = 100  # 赋值整型变量
miles = 1000.0  # 浮点型
name = "John"  # 字符串

print (counter)
print (miles)
print (name);


#python 列表

list = ['runoob', 786, 2.23, 'john', 70.2]
tinylist = [123, 'john']
print("===python列表====")
print (list)  # 输出完整列表
print (list[0])  # 输出列表的第一个元素
print (list[1:3])  # 输出第二个至第三个元素
print (list[2:])  # 输出从第三个开始至列表末尾的所有元素
print (tinylist * 2)  # 输出列表两次
print (list + tinylist)  # 打印组合的列表


#python 元组
tuple = ('runoob', 786, 2.23, 'john', 70.2)
tinytuple = (123, 'john')
print("===python元组====")
print (tuple)  # 输出完整元组
print (tuple[0])  # 输出元组的第一个元素
print (tuple[1:3])  # 输出第二个至第四个（不包含）的元素
print (tuple[2:])  # 输出从第三个开始至列表末尾的所有元素
print (tinytuple * 2)  # 输出元组两次
print (tuple + tinytuple)  # 打印组合的元组



print("===python字典====")
dict = {}
dict['one'] = "This is one"
dict[2] = "This is two"

tinydict = {'name': 'john', 'code': 6734, 'dept': 'sales'}

print (dict['one'])  # 输出键为'one' 的值
print (dict[2])  # 输出键为 2 的值
print (tinydict)  # 输出完整的字典
print (tinydict.keys())  # 输出所有键



print("===python运算符====")
a = 21
b = 10
c = 0

c = a + b
print ("1 - c 的值为：", c)

c = a - b
print ("2 - c 的值为：", c)

c = a * b
print("3 - c 的值为：", c)

c = a / b
print("4 - c 的值为：", c)

c = a % b
print ("5 - c 的值为：", c)

# 修改变量 a 、b 、c
a = 2
b = 3
c = a ** b
print ("6 - c 的值为：", c)

a = 10
b = 5
c = a // b
print ("7 - c 的值为：", c)



print("====python赋值运算符===")

a = 21
b = 10
c = 0

c = a + b
print ("1 - c 的值为：", c)

c += a
print ("2 - c 的值为：", c)

c *= a
print ("3 - c 的值为：", c)

c /= a
print ("4 - c 的值为：", c)

c = 2
c %= a
print ("5 - c 的值为：", c)

c **= a
print ("6 - c 的值为：", c)

c //= a
print ("7 - c 的值为：", c)


print("====python逻辑运算符===")
a = 10
b = 20

if a and b:
    print("1 - 变量 a 和 b 都为 true")
else:
    print("1 - 变量 a 和 b 有一个不为 true")

if a or b:
    print("2 - 变量 a 和 b 都为 true，或其中一个变量为 true")
else:
    print("2 - 变量 a 和 b 都不为 true")

# 修改变量 a 的值
a = 0
if a and b:
    print("3 - 变量 a 和 b 都为 true")
else:
    print("3 - 变量 a 和 b 有一个不为 true")

if a or b:
    print("4 - 变量 a 和 b 都为 true，或其中一个变量为 true")
else:
    print("4 - 变量 a 和 b 都不为 true")

if not (a and b):
    print("5 - 变量 a 和 b 都为 false，或其中一个变量为 false")
else:
    print("5 - 变量 a 和 b 都为 true")


print("======Python所有成员运算符========");
a = 10
b = 20
list = [1, 2, 3, 4, 5];

if (a in list):
    print("1 - 变量 a 在给定的列表中 list 中")
else:
    print("1 - 变量 a 不在给定的列表中 list 中")

if (b not in list):
    print("2 - 变量 b 不在给定的列表中 list 中")
else:
    print("2 - 变量 b 在给定的列表中 list 中")

# 修改变量 a 的值
a = 2
if (a in list):
    print("3 - 变量 a 在给定的列表中 list 中")
else:
    print("3 - 变量 a 不在给定的列表中 list 中")



print("====Python所有身份运算符===")

a = 20
b = 20

if (a is b):
    print("1 - a 和 b 有相同的标识")
else:
    print("1 - a 和 b 没有相同的标识")

if (a is not b):
    print("2 - a 和 b 没有相同的标识")
else:
    print("2 - a 和 b 有相同的标识")

# 修改变量 b 的值
b = 30
if (a is b):
    print("3 - a 和 b 有相同的标识")
else:
    print("3 - a 和 b 没有相同的标识")

if (a is not b):
    print("4 - a 和 b 没有相同的标识")
else:
    print("4 - a 和 b 有相同的标识")

    #is 与 == 区别：
# is 用于判断两个变量引用对象是否为同一个(同一块内存空间)， == 用于判断引用变量的值是否相等


# 例1：if 基本用法
print("===========if 基本用法=======")
flag = False
name = 'luren'
if name == 'python':  # 判断变量是否为 python
    flag = True  # 条件成立时设置标志为真
    print ('welcome boss')  # 并输出欢迎信息
else:
    print (name)  # 条件不成立时输出变量名称